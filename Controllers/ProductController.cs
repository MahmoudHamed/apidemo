﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using AspCoreWithAngular.Data;
using Microsoft.AspNetCore.Cors;

namespace AspCoreWithAngular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class ProductController : Controller
    {

        private readonly AppDbContext db;
        public ProductController(AppDbContext db)
        {
            this.db = db;
        }

        [HttpGet("getAll")]
        public IActionResult GetAllProducts()
        {
            var prods = db.Products.ToList();

            return Ok(prods);
        }

        [HttpGet("getById/{id?}")]
        public IActionResult GetProductById(int id)
        {
            var prod = db.Products.FirstOrDefault(givenProd => givenProd.Id == id);

            return Ok(prod);
        }



        [HttpPost("addProd")]
        public IActionResult AddProduct(Product product)
        {
            var prod = db.Products.Add(new Product() { Name = product.Name, Qty = product.Qty, Price = product.Price });
            db.SaveChanges();
            return Ok(new { key = 1, msg = "added successfully" });
        }


        [HttpPut("updateProd")]
        public IActionResult UpdateProduct(Product product)
        {
            var foundProd = db.Products.Find(product.Id);
            if (foundProd != null)
            {
                foundProd.Name = product.Name;
                foundProd.Price = product.Price;
                foundProd.Qty = product.Qty;

                db.SaveChanges();
                return Ok(new { key = 1, msg = "updated successfully" });
            }
            return Ok(new { key = 0, msg = "Product not found" });
        }


        [HttpDelete("deleteProd/{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var prod = db.Products.FirstOrDefault(prod => prod.Id == id);

            if (prod != null)
            {
                db.Products.Remove(prod);
                db.SaveChanges();
            }

            return Ok(new { key = 1, msg = "deleted successfully" });
        }



    }
}
