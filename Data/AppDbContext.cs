﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreWithAngular.Data
{
//    public class AppDbContext : DbContext
//    {
//        public AppDbContext(DbContextOptions options) : base(options)
//        {

//        }


//        public DbSet<Product> Products { get; set; }
//    }

    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        { }

        public DbSet<Product> Products { get; set; }
    }
}
